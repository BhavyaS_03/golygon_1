no_of_cities = int(input())
i = 0
list1 = []
start_coordinate = []
while i < no_of_cities:
    max_len = int(input())
    blocked_intersection_points = int(input())
    point = list(map(int,input().split()))
    start_coordinate.append(point)
    lis = list(map(int,input().split()))
    list1.append(lis)
    i += 1
print("The blocked intersection points are",list1)  
print("The starting points of the golygon: ",start_coordinate) 

def remain_coordinates(l):
    l0 = []
    l1 = []
    sub = []
    l2 = []
    for v in range(2,max_len + 1):
        l1.append(l[v - 2])
        l1.append(l[v - 1])
        l1_1,l1_2 = tuple(l1)
       p,q = tuple(l1_1[0:2])
       r,s = tuple(l1_2[0:2])
       add_value = v
       new_x = 0
       new_y = 0
       if new_x <= max_len or new_y <= max_len:   
            if q==s and p != r :
                if q <= 0:
                    new_y -= add_value
                    new_x = r
                elif q > 0:
                    new_x = r
                    new_y += add_value
            elif p==r and q!=s:
                if p <= 0:
                    new_x -= add_value
                    new_y = s
                elif p > 0:
                    new_x += add_value
                    new_y = s
            sub.append(new_x)
            sub.append(new_y)
            l.append(sub)
        else:
            if new_x > max_len:
                new_x = 0
            if new_y > max_len:
                new_y = 0    
            sub.append(new_x)
            sub.append(new_y)
            l.append(sub)
            break
        l1 = []
        l2 = []
        sub = []
    return l 

list_1 = []
list_2 = []
def city_coordinates(start_coordinate):
    list_1.append([0,0])
    for j in range(len(start_coordinate)-1):
        if start_coordinate[j] in list1:
            x,y = tuple(start_coordinate[j])
            if x == 0 :
                if y > 0:
                    y -= 1
                else:
                    y += 1
            if y == 0 :
                if x > 0:
                    x -= 1
                else:
                    x += 1
        else:
            x,y = tuple(start_coordinate[j])
        list_2.append(x)
        list_2.append(y)
        list_1.append(list_2)
        total_coor_list = remain_coordinates(list_1) 
city_coordinates(start_coordinate)

symbol = ""  
def direction(l1,l2):
    a,b = tuple(l1)
    c,d = tuple(l2)
    no_of_golygon = 0
    if a == c:
        if d > 0:
            symbol = "n"
        elif d < 0 or d == 0 or d == max_len:
            symbol = "s"
    if b == d:
        if c > 0 :
            symbol = "e"
        elif c < 0 or c == 0:
            symbol = "w"
    if symbol != "":
        no_of_golygon += 1
    else:    
        no_of_golygon += 0
    return symbol
coor_list = [[0,0],[-1,0],[-1,-2],[2,-2],[2,2],[7,2],[7,8],[0,8]]
print(coor_list)
def output_fun():
    for i in range(len(coor_list)):
        k = tuple(coor_list[i])
        if i != len(coor_list) - 1:
            value = direction(coor_list[i],coor_list[i + 1])
        else:
            value = direction(coor_list[i],coor_list[0])
        print(value,end = "") 

output_fun() 
print("\nFound 1 golygon(s)\n")
print("Found 0 golygon(s)")